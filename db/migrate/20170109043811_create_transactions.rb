class CreateTransactions < ActiveRecord::Migration[5.0]
  def change
    create_table :transactions do |t|
      t.string :borrower_name
      t.string :borrower_book
      t.datetime :borrower_date
      t.datetime :borrower_date_return

      t.timestamps
    end
  end
end
