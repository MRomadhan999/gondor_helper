json.extract! transaction, :id, :borrower_name, :borrower_book, :borrower_date, :borrower_date_return, :created_at, :updated_at
json.url transaction_url(transaction, format: :json)